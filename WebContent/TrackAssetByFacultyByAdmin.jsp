<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, org.inventory.dao.*, org.inventory.jdo.*" %>

<html>
<jsp:include page="Header.jsp">
   	<jsp:param value="test" name="test"/>
</jsp:include>
<body class="bg-steel">
	<div style="width:85%; margin:0 auto;">
    <jsp:include page="Menus.jsp">
    	<jsp:param value="test" name="test"/>
	</jsp:include>

    <div class="page-content">
        <div class="flex-grid no-responsive-future" style="height: 100%;">
            <div class="row" style="height: 100%">
                <div class="cell auto-size padding20 bg-white" id="cell-content">
<%
	String action = request.getParameter("action");
	if(action == null)
	{
%>
		<h1 class="text-light"> Track Assets By Faculty </h1>
		<hr class="thin bg-grayLighter">
		<br>
		<form method="post" action="./TrackAssetByFacultyByAdmin.jsp" data-role="validator">
		<input type="hidden" name="action" id="action" value="TrackAsset" />
		<table style="width:70%;">
			<tr>
				<td width="25%">Select Faculty</td>
				<td width="50%">
					<select style="width:250px;" id="faculty_name" name="faculty_name">
<%
					List<String> fList = InventoryDao.getFacultyList();
					for(String faculty : fList)
					{
						out.println("<option>"+faculty+"</option>");
					}
%>
					</select>
				</td>
				<td><button class="button primary" > GO </button></td>
			<tr>
		</table>
		</form>
<%
	}
	else
	{
		String facultyName = request.getParameter("faculty_name");
		System.out.println("facultyName = "+facultyName);
		List<Inventory> invList = InventoryDao.getInventoryListByFaculty(facultyName);
%>
		<h1 class="text-light"> Track Assets By Faculty </h1>
		<hr class="thin bg-grayLighter">
		
		<table class="dataTable border bordered" data-role="datatable" data-auto-width="false">
		    <thead>
		    <tr>
		        <td>Asset No</td>
		        <td>CP Model No</td>
		        <!-- 
		        <td>Serial No</td>
		        <td>Own Tag</td>
		        <td>Acq Date</td>
		        <th>Total Cost</th>
		        <td>Purpose</td>
		        <td>Description</td>
		        <td>Dept-Sub</td>
		         -->
		        <td>Assigned To</td>
		    </tr>
		    </thead>
		    <tbody>
<%
			for(Inventory inv : invList)
			{
				out.println("<tr>");
				out.println("<td>"+inv.getAssetNumber()+"</td>");
				out.println("<td>"+inv.getCpModelNo()+"</td>");
				out.println("<td>"+inv.getAssignedTo()+"</td>");
				out.println("</tr>");
			}
%>
		    </tbody>
		</table>

<%
	}
%>                    
                    
                </div>
            </div>
        </div>
    </div>
    </div>
</body>
</html>