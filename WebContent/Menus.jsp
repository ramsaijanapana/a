<%
	String usertype = (String)session.getAttribute("usertype");
	long lastaccessed = session.getLastAccessedTime();
	long current = new java.util.Date().getTime();
	
	long diff = (current-lastaccessed)/1000/60;
	//System.out.println("Accessed "+diff+" mins back.");
%>
<div class="app-bar fixed-top darcula" data-role="appbar" style="width:85%; margin:0 auto;">
    <a class="app-bar-element branding">Inventory</a>
    <span class="app-bar-divider"></span>
    <ul class="app-bar-menu" style="height: 30px;">
    	<%
		if("Faculty".equals(usertype))
		{
		%>
        <li>
            <a href="" class="dropdown-toggle">Assets</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="./NewAssetRequest.jsp">New Asset Request</a></li>
                <li class="divider"></li>
                <li><a href="./CurrentAssets.jsp">Current Assets</a></li>
                <li class="divider"></li>
                <li><a href="./History.jsp">History</a></li>
            </ul>
        </li>
        <%
		} else if("Admin".equals(usertype))
		{
		%>
        <li>
            <a href="" class="dropdown-toggle">Assets</a>
            <ul class="d-menu" data-role="dropdown">
            	<li><a href="./NewAssetRequests.jsp">Asset Requests</a></li>
                <li class="divider"></li>
                <li><a href="./TrackAssetByFacultyByAdmin.jsp">Track Asset By Faculty</a></li>
                <li class="divider"></li>
                <li><a href="./TrackAssetByIdByAdmin.jsp">Track Asset By Id</a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <%
		} else if("head".equals(usertype))
		{
		%>
        <li>
            <a href="" class="dropdown-toggle">Assets</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="./TrackAssetByFacultyByHead.jsp">Track Asset By Faculty</a></li>
                <li class="divider"></li>
                <li><a href="./TrackAssetByIdByHead.jsp">Track Asset By Id</a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <%
		}
        %>
        <li>
            <a href="" class="dropdown-toggle">Help</a>
            <ul class="d-menu" data-role="dropdown">
                <li><a href="./About.jsp">About</a></li>
            </ul>
        </li>
    </ul>

	<div class="app-bar-element place-right">
	<%
		
		if(usertype == null)
		{
	%>
			<span class="dropdown-toggle"> Login </span>
			<div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" 
				data-role="dropdown" data-no-close="true" style="width: 350px">
			   
			   <h3 class="text-light">Login</h3>
			   <form method="post" action="./Login.jsp" data-role="validator">
			   <table>
			   <tr><td width="30%">User Name</td><td width="10%">:</td><td width="60%">
			   		<div class="input-control text">
				   		<input type="text" name="user_name" id="user_name" 
				   			style="width:150px; height: 30px;"
				   			data-validate-func="minlength" data-validate-arg="4"
		            		data-validate-hint="This field must contains min 4 characters.">
	            	</div>
			   		</td></tr>
			   <tr><td>Password</td><td>:</td><td>
			   		<div class="input-control text">
				   		<input type="password" name="user_password" id="user_password" 
				   			style="width:150px; height: 30px;"
				   			data-validate-func="minlength" data-validate-arg="4"
		            		data-validate-hint="This field must contains min 4 characters.">
			   		</div>
			   		</td></tr>
			   <tr><td></td><td></td><td>
			   	<button class="button primary" > Login </button>
			    </td></tr>
			    </table>
			    <a href="./Register.jsp">Register Here</a>
			    </form>
			</div>
	<%
		}
		else
		{
	%>
			<a class="app-bar-element" href="./Logout.jsp">Logout</a>
	<%
		}
	%>
		
	</div>
        
</div>